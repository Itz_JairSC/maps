package com.example.maps;

public class MapsAPI {
    private double Latitud;
    private double Longitud;

    public MapsAPI() {
    }

    public double getLatitud() {
        return Latitud;
    }

    public void setLatitud(double latitud) {
        Latitud = latitud;
    }

    public double getLongitud() {
        return Longitud;
    }

    public void setLongitud(double longitud) {
        Longitud = longitud;
    }
}
